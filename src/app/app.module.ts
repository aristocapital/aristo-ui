import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {HttpModule} from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { AmChartsModule } from "@amcharts/amcharts3-angular";

import { LocalStorageService } from './services/localstorage/local-storage.service'

import { RestService } from './services/networkmanager/core/rest.service';
import { ROUTING } from './app.routing'
import { AppComponent } from './app.component';
import { TokenInterceptor } from './services/networkmanager/core/token.interceptor';
import { AppBtnPrimaryComponent } from './views/components/abstract/app-btn-primary/app-btn-primary.component';
import { LoaderComponent } from './views/components/abstract/loader/loader.component';
import { GenericModalComponent } from './views/components/abstract/generic-modal/generic-modal.component';
import { ModalModule, CollapseModule } from "ngx-bootstrap";
import { TwoPartitionModalComponent } from './views/components/abstract/two-partition-modal/two-partition-modal.component';
import { AuthModalComponent } from './views/components/auth-modal/auth-modal.component';
import { UserHomeComponent } from './views/pages/user-home/user-home.component';
import { P404Component } from './views/pages/p404/p404.component';
import { LandingPageComponent } from './views/pages/landing-page/landing-page.component';
import { KycComponent } from './views/components/kyc/kyc.component';
import { OnboardingComponent } from './views/components/onboarding/onboarding.component';
import { PlainModalComponent } from './views/components/abstract/plain-modal/plain-modal.component';
import { DepositBtcComponent } from './views/components/deposit-btc/deposit-btc.component';
import { GenericCardComponent } from './views/components/abstract/generic-card/generic-card.component';
import { BuySellNavCardComponent } from './views/components/buy-sell-nav-card/buy-sell-nav-card.component';
import { DepositWithdrawalComponent } from './views/pages/deposit-withdrawal/deposit-withdrawal.component';
import { BtcDepositWithdrawalComponent } from './views/components/btc-deposit-withdrawal/btc-deposit-withdrawal.component';
import { FormErrorComponent } from './views/components/abstract/form-error/form-error.component';
import { SignInComponent } from './views/components/auth/sign-in/sign-in.component';
import { SignUpComponent } from './views/components/auth/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './views/components/auth/forgot-password/forgot-password.component';
import { NavGrowthCardComponent } from './views/components/nav-growth-card/nav-growth-card.component';
import { UserBalanceCardComponent } from './views/components/user-balance-card/user-balance-card.component';
import { VerifyOtpComponent } from './views/components/auth/verify-otp/verify-otp.component';
import { ConfirmationContentComponent } from './views/components/auth/confirmation-content/confirmation-content.component';
import { ResetPasswordComponent } from './views/components/auth/reset-password/reset-password.component';
import { HeaderComponent } from './views/components/abstract/header/header.component';
import { CopyrightFooterComponent } from './views/components/abstract/copyright-footer/copyright-footer.component';
import { NavTxnHistoryComponent } from './views/components/nav-txn-history/nav-txn-history.component';
import { BtcTxnHistoryComponent } from './views/components/btc-txn-history/btc-txn-history.component';

@NgModule({
  declarations: [
    AppComponent,
    AppBtnPrimaryComponent,
    LoaderComponent,
    GenericModalComponent,
    TwoPartitionModalComponent,
    AuthModalComponent,
    UserHomeComponent,
    P404Component,
    LandingPageComponent,
    KycComponent,
    OnboardingComponent,
    PlainModalComponent,
    DepositBtcComponent,
    GenericCardComponent,
    BuySellNavCardComponent,
    DepositWithdrawalComponent,
    BtcDepositWithdrawalComponent,
    FormErrorComponent,
    SignInComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    NavGrowthCardComponent,
    UserBalanceCardComponent,
    VerifyOtpComponent,
    ConfirmationContentComponent,
    ResetPasswordComponent,
    HeaderComponent,
    CopyrightFooterComponent,
    NavTxnHistoryComponent,
    BtcTxnHistoryComponent
  ],
  imports: [
    BrowserModule
    ,HttpClientModule
    ,FormsModule
    ,ReactiveFormsModule
    ,ModalModule.forRoot()
    ,CollapseModule.forRoot()
    ,ROUTING
    ,NgxQRCodeModule
    ,AmChartsModule
  ],
  providers: [
      {
        provide: HTTP_INTERCEPTORS,
        useClass: TokenInterceptor,
        multi: true
      },
      LocalStorageService,
      RestService
  ],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
