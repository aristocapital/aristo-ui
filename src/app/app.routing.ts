import { RouterModule, Routes } from '@angular/router';

import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';
import { UserHomeComponent } from './views/pages/user-home/user-home.component';
import { P404Component } from './views/pages/p404/p404.component';
import { LandingPageComponent } from './views/pages/landing-page/landing-page.component';
import { AppComponent } from './app.component';
import { DepositWithdrawalComponent } from './views/pages/deposit-withdrawal/deposit-withdrawal.component'

export const AppRoutes: Routes = [
  { path: '', component: LandingPageComponent },
  { path: 'home', component: UserHomeComponent },
  { path: 'depositAndWithdrawl', component: DepositWithdrawalComponent },
  { path: '**', component: P404Component }
];


export const ROUTING: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
