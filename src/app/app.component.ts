import { Component, Output, EventEmitter } from '@angular/core';
import { LocalStorageService } from './services/localstorage/local-storage.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ LocalStorageService ]
})
export class AppComponent {
  title = 'app';

  constructor(private localStorage: LocalStorageService ) {}

  ngOnInit() {
    this.localStorage.setFirstName("Sagar");
    this.localStorage.setAuthToken("eu320dkk33ew039ejad2eqawdasdasd");
  }

}
