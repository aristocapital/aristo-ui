import { Injectable } from '@angular/core';
import { RestService } from './core/rest.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class AuthService {

  private HOST: string = environment.API_HOST;

  constructor(private restService: RestService){}

  login = (username: String, password: String) => {
    let body = {"username": username, "password": password} ;
    let t = this.restService.post(this.HOST+"/profile", body, {});
    return t;
  }

  register = (name: String, email: String, mobile: String, password: String) => {
    let body = {"name": name, "email": email, "mobile": mobile, "password": password} ;
    let t = this.restService.post(this.HOST+"/register", body, {});
    return t;
  }

  verifyOtp = (email: String, otp: number) => {
    let body = {"email": email, "otp": otp} ;
    let t = this.restService.post(this.HOST+"/verify_otp", body, {});
    return t;
  }

  forgotPassword = (email: String) => {
    let body = {"email": email} ;
    let t = this.restService.post(this.HOST+"/forgot_password", body, {});
    return t;
  }

  resetPassword = (newPassword: String) => {
    let body = {"newPassword": newPassword} ;
    let t = this.restService.post(this.HOST+"/reset_password", body, {});
    return t;
  }

}
