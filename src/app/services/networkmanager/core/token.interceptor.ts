import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';

import { LocalStorageService } from '../../localstorage/local-storage.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  private localStore: LocalStorageService;

  constructor() {
      this.localStore = new LocalStorageService();
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if(this.localStore.getAuthToken()){

      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.localStore.getAuthToken()}`
        }
      });
    }
    return next.handle(request);
  }
}
