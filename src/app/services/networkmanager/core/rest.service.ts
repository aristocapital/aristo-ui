import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import 'rxjs/add/operator/map';
import {
  Response,
  RequestOptions,
  Headers
} from '@angular/http';

@Injectable()
export class RestService {

  constructor(private http: HttpClient) { }

	post(url: string, body: any, headers: any) {
    return this.http.post(url, body, headers);

	}

	get(url: string, body: any): any {
    return this.http.get(url, body);
	}

	patch(url: string, body: any) {
    return this.http.patch(url, body);
	}
  
	private logError(error: any) {
		console.error(error.error);
		throw error;
	}
}
