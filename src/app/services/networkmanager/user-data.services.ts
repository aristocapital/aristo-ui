import { Injectable } from '@angular/core';
import { RestService } from './core/rest.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class UserDataService {

  private HOST: string = environment.API_HOST;

  constructor(private restService: RestService){}

  getBalances = () => {
    let t = this.restService.get(this.HOST+"/user_balances", {});
    return t;
  }

  getBTCDepositAddress = () => {
    let t = this.restService.get(this.HOST+"/get-deposit-btc-address", {});
    return t;
  }

  buyA10Token = (buyAmount: number) => {
    let body = { "exchangeCurrType":"BTC","exchangeCurrAmount":buyAmount}
    let t = this.restService.post(this.HOST+"/buya10", body, {});
    return t;
  }

  sellA10Token = (a10TokenToSell: number) => {
    let body = { "exchangeCurrType":"NAV","unitsToSell":a10TokenToSell}
    let t = this.restService.post(this.HOST+"/buya10", body, {});
    return t;
  }

  withdrawBTC = (btcAmount: number, addressToWithdraw: string) => {
    let body = { "currencyToWithdraw":"BTC","amountToWithdraw":btcAmount, "addressToWithdrawTo": addressToWithdraw}
    let t = this.restService.post(this.HOST+"/buya10", body, {});
    return t;
  }

  logout = () => {
    let t = this.restService.post(this.HOST+"/logout", {}, {});
    return t;
  }

  getA10History = (start: number, count: number ) => {
    let body = {"start": start, "count": count};
    let t = this.restService.get(this.HOST+"/get-a10-history", body);
    return t;
  }

  getBTCHistory = (start: number, count: number ) => {
    let body = {"start": start, "count": count};
    let t = this.restService.get(this.HOST+"/get-btc-history", body);
    return t;
  }

}
