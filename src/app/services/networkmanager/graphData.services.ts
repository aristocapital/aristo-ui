import { Injectable } from '@angular/core';
import { RestService } from './core/rest.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class GraphDataService {

  private HOST: string = environment.API_HOST;

  constructor(private restService: RestService){}

  getBtcA10Comparison = () => {
    let t = this.restService.get(this.HOST+"/btc_a10", {});
    return t;
  }

  getNavGrowthData = () => {
    let t = this.restService.get(this.HOST+"/navgrowth", {});
    return t;
  }

  getNavCompositionData = () => {
    let t = this.restService.get(this.HOST+"/navdistribution", {});
    return t;
  }

}
