import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

  AUTH_TOKEN: string = 'Authorization';
  FIRST_NAME: string = 'UserFirstName';

  constructor() { }

  public getAuthToken(): string {
    return sessionStorage.getItem(this.AUTH_TOKEN);
  }

  public setAuthToken(token: string): void {
    sessionStorage.setItem(this.AUTH_TOKEN, token);
  }

  public removeAuthToken(): void {
    sessionStorage.removeItem(this.AUTH_TOKEN);
  }

  public getFirstName(): string {
    return sessionStorage.getItem(this.FIRST_NAME);
  }

  public setFirstName(firstName: string): void {
    sessionStorage.setItem(this.FIRST_NAME, firstName);
  }

  public clear(): void{
    localStorage.clear();
    sessionStorage.clear();
  }

}
