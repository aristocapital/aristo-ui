import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { AuthModalComponent } from '../../components/auth-modal/auth-modal.component';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { GraphDataService } from "../../../services/networkmanager/graphData.services"
import { AuthDialogState } from '../../components/auth-modal/auth-dialog-state';
import { Router } from "@angular/router";
import { LocalStorageService } from '../../../services/localstorage/local-storage.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
  providers:[ GraphDataService ]
})
export class LandingPageComponent implements OnInit {


  @ViewChild('authModal') authModal: AuthModalComponent ;
  isCollapsed: boolean = true;
  openFaqIndex  = 0;
  teamMembers: any[];
  faqs: any[];
  public options: any;
  private chart: AmChart;
  authDialogStateEnum =  AuthDialogState;

  constructor(private localStore: LocalStorageService,
              private router: Router,
              private amChartsService: AmChartsService,
              private graphDataService: GraphDataService) {}

  ngOnInit() {
    if(this.localStore.getAuthToken()){
      this.router.navigate(['/home']);
    }
    this.teamMembers = [
      {
        "image" : "assets/images/group-3@2x.png",
        "name" : "Abdul Khalid",
        "desc" : "Passionate, compassionate and denent guy"
      },
      {
        "image" : "assets/images/group-3@2x.png",
        "name" : "Ashish Garg",
        "desc" : "Passionate, compassionate and decent guy"
      },
      {
        "image" : "assets/images/group-3@2x.png",
        "name" : "Rhythm Gupta",
        "desc" : "Passionate, compassionate and decent guy"
      },
      {
        "image" : "assets/images/group-3@2x.png",
        "name" : "Sagar Patidar",
        "desc" : "Passionate, compassionate and best guy ever!"
      },
      {
        "image" : "assets/images/group-3@2x.png",
        "name" : "Yasoob Haider",
        "desc" : "Passionate, compassionate and decent guy"
      }
    ];

    this.faqs = [
      {
        "question": "Q1. What is Aristo Capital?",
        "answer" : "Aristo Capital is India’s leading crypto based mutual fund. Our mission is to enable average investors participate in this blockchain revolution by providing them appropriate financial instruments. <br/><br/>Our first fund is an Index fund. We believe that indices are one of the most important tool built in 20th century which allow everyday investors to understand the direction of market and they serve as a health indicator of the underlying asset class.",
        "isCollapsed": false
      },
      {
        "question": "Q2. What is the process to invest in your A10? How can I start?",
        "answer" : "Aristo Capital is India’s leading crypto based mutual fund. Our mission is to enable average investors participate in this blockchain revolution by providing them appropriate financial instruments. <br/><br/>Our first fund is an Index fund. We believe that indices are one of the most important tool built in 20th century which allow everyday investors to understand the direction of market and they serve as a health indicator of the underlying asset class.",
        "isCollapsed": true
      },
      {
        "question": "Q3. What is the fees that you guys will charge for it?",
        "answer" : "Aristo Capital is India’s leading crypto based mutual fund. Our mission is to enable average investors participate in this blockchain revolution by providing them appropriate financial instruments. <br/><br/>Our first fund is an Index fund. We believe that indices are one of the most important tool built in 20th century which allow everyday investors to understand the direction of market and they serve as a health indicator of the underlying asset class.",
        "isCollapsed": true
      }
    ];

    this.options = this.makeOptions([]);

  }

  ngAfterViewInit() {
    this.chart = this.amChartsService.makeChart('chartdiv', this.options);

    this.graphDataService.getBtcA10Comparison().subscribe((response)=>{
      this.amChartsService.updateChart(this.chart, () => {
        // Change whatever properties you want, add event listeners, etc.
        this.chart.dataProvider = response.data;

      });
    });

  }

  ngOnDestroy() {
    if (this.chart) {
      this.amChartsService.destroyChart(this.chart);
    }
  }

  makeOptions = (dataProvider) => {
    return {
      'type': 'serial',
      'theme': 'light',
      'marginTop': 30,
      'marginRight': 80,
      'dataProvider': dataProvider,
      'valueAxes': [{
        'axisColor': '#f5f4f5',
        'position': 'left',
        'minMaxMultiplier': 1.2,
        'title': 'Price ($)',
        'color':'#f5f4f5',
        'axisTitleOffset': 20,
        'gridColor': '#f5f4f5',
        'titleColor': '#fff'
      }],
      'graphs': [{
        'id': 'g1',
        'balloonText': '[[category]]<br><b><span style=\'font-size:14px;\'>[[value]]</span></b>',
        'bullet': 'round',
        'bulletSize': 1,
        'lineColor': '#f5f4f5',
        'lineThickness': 2,
        'valueField': 'value1'
      },
      {
        'id': 'g2',
        'balloonText': '[[category]]<br><b><span style=\'font-size:14px;\'>[[value]]</span></b>',
        'bullet': 'round',
        'bulletSize': 1,
        'lineColor': '#f7ff30',
        'lineThickness': 2,
        'valueField': 'value2'
      }],
      'chartCursor': {
        'categoryBalloonDateFormat': 'YYYY',
        'cursorAlpha': 0,
        'valueLineEnabled': true,
        'valueLineBalloonEnabled': true,
        'valueLineAlpha': 0.5,
        'fullWidth': true
      },
      'dataDateFormat': 'YYYY',
      'categoryField': 'year',
      'categoryAxis': {
        'minPeriod': 'YYYY',
        'parseDates': true,
        "gridThickness": 0,
        "axisColor": '#f5f4f5',
        'color': '#fff',
        'title': "Time"
      }
    };
  }

  openSignUpDialog = () => {
    this.authModal.open(AuthDialogState.SIGN_UP);
  }

  openSignInDialog = () => {
    this.authModal.open(AuthDialogState.SIGN_IN);
  }

}
