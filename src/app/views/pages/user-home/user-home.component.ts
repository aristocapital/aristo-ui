import { Component, OnInit } from '@angular/core';
import { Tab, Tabs } from '../../components/abstract/generic-card/tabs';
import { Router } from "@angular/router";
import { LocalStorageService } from '../../../services/localstorage/local-storage.service';

@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.scss']
})
export class UserHomeComponent implements OnInit {

  constructor(private localStore: LocalStorageService,
              private router: Router) { }

  ngOnInit() {
    if(!this.localStore.getAuthToken()){
      this.router.navigate(['/home']);
    }
  }

}
