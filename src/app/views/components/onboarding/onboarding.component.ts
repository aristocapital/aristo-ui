import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LocalStorageService } from '../../../services/localstorage/local-storage.service'

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.scss']
})
export class OnboardingComponent implements OnInit {

  firstName: string;

  constructor(private localStroageService: LocalStorageService) {
  }

  ngOnInit() {
    this.firstName = this.localStroageService.getFirstName();
  }

}
