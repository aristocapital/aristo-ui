import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from "@angular/router";

import { AppButtonState } from '../../abstract/app-btn-primary/app-btn-state';
import { AuthValidators } from '../auth.validation'
import { AuthService } from '../../../../services/networkmanager/auth.services';
import { AuthDialogState } from '../../auth-modal/auth-dialog-state';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  providers: [ AuthService ]
})
export class SignInComponent implements OnInit {

  @Input() onClickForgotPassword: Function;
  @Input() onClickSignUp: Function;
  @Output() authButtonStateEvent = new EventEmitter<AppButtonState>();
  @Output() onSuccess = new EventEmitter<any>();

  signInButtonState: AppButtonState;
  form: FormGroup ;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
     'email': ['', [ AuthValidators.emailValidation ]],
     'password': ['', [ AuthValidators.passwordValidation ]],
   });
  }

  changeButtonState = (newState) => {
    this.signInButtonState = newState;
    this.authButtonStateEvent.emit(this.signInButtonState);
  }

  login = () => {
    this.changeButtonState(AppButtonState.PROCESSING);
    this.authService.login(
      this.form.controls['email'].value,
      this.form.controls['password'].value).subscribe(
        (response) => {
          console.log("Sign in response:: ",response);
          this.changeButtonState(AppButtonState.ENABLED);
          // this.router.navigate(['/home']);
          this.onSuccess.emit({dialogType: AuthDialogState.SIGN_IN, otpSourceId: this.form.controls['email'].value});
        }
      );

  }

}
