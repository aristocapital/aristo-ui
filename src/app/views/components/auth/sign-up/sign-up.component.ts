import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppButtonState } from '../../abstract/app-btn-primary/app-btn-state';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { AuthValidators } from '../auth.validation';
import { AuthService } from '../../../../services/networkmanager/auth.services';
import { AuthDialogState } from '../../auth-modal/auth-dialog-state';
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
  providers: [ AuthService ]
})
export class SignUpComponent implements OnInit {

  @Input() onClickSignIn: Function;

  signUpButtonState: AppButtonState;
  @Output() authButtonStateEvent = new EventEmitter<AppButtonState>();
  @Output() onSuccess = new EventEmitter<any>();

  form: FormGroup ;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'name': ['', [ Validators.required, AuthValidators.nameValidation ]],
      'mobile': ['', [ Validators.required, AuthValidators.mobileValidation ]],
      'email': ['', [ Validators.required, AuthValidators.emailValidation ]],
      'password': ['', [ Validators.required, AuthValidators.passwordValidation ]],
   });
  }

  changeButtonState = (newState) => {
    this.signUpButtonState = newState;
    this.authButtonStateEvent.emit(this.signUpButtonState);
  }

  register = () => {
    this.changeButtonState(AppButtonState.PROCESSING);
    this.authService.register(
      this.form.controls['name'].value,
      this.form.controls['email'].value,
      this.form.controls['mobile'].value,
      this.form.controls['password'].value).subscribe(
        (response) => {
          console.log("Sign up response:: ",response);
          this.changeButtonState(AppButtonState.ENABLED);
          // this.router.navigate(['/home']);
          this.onSuccess.emit({dialogType: AuthDialogState.SIGN_UP, otpSourceId: this.form.controls['email'].value});
        }
    );

  }


}
