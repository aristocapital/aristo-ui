import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from "@angular/router";

import { AppButtonState } from '../../abstract/app-btn-primary/app-btn-state';
import { AuthValidators } from '../auth.validation'
import { AuthService } from '../../../../services/networkmanager/auth.services';
import { AuthDialogState } from '../../auth-modal/auth-dialog-state';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  providers: [ AuthService ]
})
export class ResetPasswordComponent implements OnInit {

  resetButtonState: AppButtonState;
  form: FormGroup ;

  @Output() authButtonStateEvent = new EventEmitter<AppButtonState>();
  @Output() onSuccess = new EventEmitter<any>();

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
     'password': ['', [ AuthValidators.passwordValidation ]],
     'confirmPassword': ['', [ AuthValidators.passwordValidation ]],
   });
  }

  changeButtonState = (newState) => {
    this.resetButtonState = newState;
    this.authButtonStateEvent.emit(this.resetButtonState);
  }

  resetPassword = () => {
    this.changeButtonState(AppButtonState.PROCESSING);
    let newPassword = this.form.controls['password'].value;
    let newPasswordConfirmation = this.form.controls['confirmPassword'].value;

    if(newPassword === newPasswordConfirmation){

      this.authService.resetPassword(newPassword).subscribe(
        (response) => {
          console.log("Reset Password response:: ",response);
          this.changeButtonState(AppButtonState.ENABLED);
          // this.router.navigate(['/home']);
          this.onSuccess.emit({dialogType: AuthDialogState.RESET_PASSWORD});
        }
      );

    }

  }

}
