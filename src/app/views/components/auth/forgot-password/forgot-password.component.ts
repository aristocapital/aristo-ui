import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppButtonState } from '../../abstract/app-btn-primary/app-btn-state';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthValidators } from '../auth.validation';
import { AuthService } from '../../../../services/networkmanager/auth.services';
import { AuthDialogState } from '../../auth-modal/auth-dialog-state';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
  providers: [ AuthService ]
})
export class ForgotPasswordComponent implements OnInit {

  forgotPasswordButtonState: AppButtonState;
  @Output() authButtonStateEvent = new EventEmitter<AppButtonState>();
  @Output() onSuccess = new EventEmitter<any>();

  form: FormGroup ;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'email': ['', [ Validators.required, AuthValidators.emailValidation ]]
   });
  }

  changeButtonState = (newState) => {
    this.forgotPasswordButtonState = newState;
    this.authButtonStateEvent.emit(this.forgotPasswordButtonState);
  }

  forgotPassword = () => {
    this.changeButtonState(AppButtonState.PROCESSING);
    this.authService.forgotPassword(
      this.form.controls['email'].value).subscribe(
        (response) => {
          console.log("forgot password success");
          this.changeButtonState(AppButtonState.ENABLED);
          // this.onSuccess.emit({"dialogType": AuthDialogState.FORGOT_PASSWORD});
          this.onSuccess.emit({dialogType: AuthDialogState.FORGOT_PASSWORD});
        }
    );
  }

}
