import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmationContentComponent } from './confirmation-content.component';

describe('ConfirmationContentComponent', () => {
  let component: ConfirmationContentComponent;
  let fixture: ComponentFixture<ConfirmationContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmationContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
