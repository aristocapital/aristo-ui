import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-confirmation-content',
  templateUrl: './confirmation-content.component.html',
  styleUrls: ['./confirmation-content.component.scss']
})
export class ConfirmationContentComponent implements OnInit {

  _textContent: String;

  constructor() { }

  ngOnInit() {
  }

  @Input()
  set textContent(value: string) {
    this._textContent = value;
  }

}
