import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { AppButtonState } from '../../abstract/app-btn-primary/app-btn-state';
import { AuthValidators } from '../auth.validation';
import { AuthService } from '../../../../services/networkmanager/auth.services';

@Component({
  selector: 'app-verify-otp',
  templateUrl: './verify-otp.component.html',
  styleUrls: ['./verify-otp.component.scss'],
  providers: [ AuthService ]
})
export class VerifyOtpComponent implements OnInit {

  _otpSourceId: String;
  @Input() onClickVerifyOtp: Function;

  verifyOtpButtonState: AppButtonState;
  @Output() authButtonStateEvent = new EventEmitter<AppButtonState>();

  form: FormGroup ;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'otp': ['', [ Validators.required, AuthValidators.otpValidation ]]
   });
  }

  changeButtonState = (newState) => {
    this.verifyOtpButtonState = newState;
    this.authButtonStateEvent.emit(this.verifyOtpButtonState);
  }

  @Input()
  set otpSourceId(value: string) {
    this._otpSourceId = value;
  }

  verifyOtp = () => {
    this.changeButtonState(AppButtonState.PROCESSING);
    this.authService.verifyOtp(
      this.otpSourceId,
      this.form.controls['otp'].value).subscribe(
        (response) => {
          this.changeButtonState(AppButtonState.ENABLED);
          this.router.navigate(['/home']);
        }
    );
  }

}
