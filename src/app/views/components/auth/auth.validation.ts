import { FormControl, FormGroup, ValidationErrors } from '@angular/forms';

export class AuthValidators {

  static emailValidation(c: FormControl): ValidationErrors {
    const emailStr = c.value;
    const regex = /\S+@\S+\.\S+/;
    let isValid = false;
    if(emailStr){
      isValid = regex.test(emailStr);
    }
    const message = {
      'email': {
        'message' : 'Invalid email'
      }
    };
    return isValid ? null : message;
  }

  static passwordValidation(c: FormControl): ValidationErrors {
    let password = c.value;
    let isValid = false;
    if(password && password.length>6){
      isValid = true;
    }
    const message = {
      'email': {
        'message' : 'Weak Password. Please use a secure password.'
      }
    };
    return isValid ? null : message;
  }

  static nameValidation(c: FormControl): ValidationErrors {
    let name = c.value;
    let isValid = false;
    if(name && name.length>3){
      isValid = true;
    }
    const message = {
      'email': {
        'message' : 'Please enter your full name'
      }
    };
    return isValid ? null : message;
  }

  static mobileValidation(c: FormControl): ValidationErrors {
    let mobile = c.value;
    let isValid = false;
    if(mobile && mobile.length===10){
      isValid = true;
    }
    const message = {
      'email': {
        'message' : 'Please enter a valid mobile number'
      }
    };
    return isValid ? null : message;
  }

  static otpValidation(c: FormControl): ValidationErrors {
    let otp = c.value;
    let isValid = false;
    if(otp && otp>99999 && otp<1000000 ){
      isValid = true;
    }
    const message = {
      'email': {
        'message' : 'Please enter a valid OTP'
      }
    };
    return isValid ? null : message;
  }

}
