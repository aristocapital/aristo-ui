import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Tab, Tabs } from '../abstract/generic-card/tabs';
import { UserDataService  } from '../../../services/networkmanager/user-data.services';
import { AppButtonState } from '../../components/abstract/app-btn-primary/app-btn-state';
import { UserBalanceDto } from '../../../dto/user-balance.dto';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-btc-deposit-withdrawal',
  templateUrl: './btc-deposit-withdrawal.component.html',
  styleUrls: ['./btc-deposit-withdrawal.component.scss'],
  providers: [ UserDataService ]
})
export class BtcDepositWithdrawalComponent implements OnInit {

  tabs: Tabs = new Tabs(["Deposit BTC", "Withdraw BTC"]);

  userBtcAddress: string;

  userBalances: UserBalanceDto;
  depositLoader: boolean = false;
  withdrawButtonState: AppButtonState = AppButtonState.DISABLED;
  @Output() withdrawButtonStateEvent = new EventEmitter<AppButtonState>();
  withdrawLoader: boolean = false;
  withdrawBtcAmount: number = 0;
  withdrawBtcAddress: string = '';
  withdrawError: string;

  form: FormGroup ;

  constructor(private formBuilder: FormBuilder,
              private userDataService: UserDataService) { }

  ngOnInit() {
    this.getUserBalances();
    this.getUserBTCDepositAddress();
    this.changeWithdrawButtonState(AppButtonState.DISABLED);
    this.form = this.formBuilder.group({
      'btcAmount': ['', [ Validators.required ]],
      'addressToWithdraw': ['', [ Validators.required]]
   });
  }

  changeWithdrawButtonState = (newState) => {
    this.withdrawButtonState = newState;
    this.withdrawButtonStateEvent.emit(this.withdrawButtonState);
  }


  getUserBalances = () => {
    this.withdrawLoader = true;
    this.userDataService.getBalances().subscribe(
        (response) => {
          this.userBalances = new UserBalanceDto(response.data.a10_balance,
                                            response.data.btc_balance,
                                            response.data.a10_price_in_btc);
          this.changeWithdrawButtonState(AppButtonState.ENABLED);
          this.withdrawLoader = false;
        }
    );
  }

  getUserBTCDepositAddress = () => {
    this.depositLoader = true;
    this.userDataService.getBTCDepositAddress().subscribe(
        (response) => {
          this.userBtcAddress = response.data.publicKey;
          this.changeWithdrawButtonState(AppButtonState.ENABLED);
          this.depositLoader = false;
        }
    );
  }

  onWithdrawAmountChange = (value: number) =>{
    this.withdrawBtcAmount = value;
    this.checkFormValidation()
  }

  onWithdrawAddressChange = (value: string) =>{
    this.withdrawBtcAddress = value;
    this.checkFormValidation()
  }

  checkFormValidation = () => {
    if((!this.withdrawBtcAddress || this.withdrawBtcAddress.length===0) || this.withdrawBtcAmount === 0){
      this.changeWithdrawButtonState(AppButtonState.DISABLED);
      return false;
    }
    else if(this.withdrawBtcAmount > this.userBalances.btcBalance){
      this.withdrawError = "You have exceeded your BTC balance";
      this.changeWithdrawButtonState(AppButtonState.DISABLED);
      return false;
    }else{
      this.changeWithdrawButtonState(AppButtonState.ENABLED);
      this.withdrawError = null;
      return true;
    }
  }


  withdrawBTC = () => {
    if(this.checkFormValidation()){
      this.changeWithdrawButtonState(AppButtonState.PROCESSING);
      this.userDataService.withdrawBTC(
        this.form.controls['btcAmount'].value,
        this.form.controls['addressToWithdraw'].value
      ).subscribe(
          (response) => {
            this.withdrawBtcAmount = 0;
            this.changeWithdrawButtonState(AppButtonState.DISABLED);
            alert("Your withdraw BTC request has been received. Thanks");
          }
      );
    }
  }

}
