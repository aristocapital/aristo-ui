import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtcDepositWithdrawalComponent } from './btc-deposit-withdrawal.component';

describe('BtcDepositWithdrawalComponent', () => {
  let component: BtcDepositWithdrawalComponent;
  let fixture: ComponentFixture<BtcDepositWithdrawalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtcDepositWithdrawalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtcDepositWithdrawalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
