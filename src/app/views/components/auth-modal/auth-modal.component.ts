import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms';
import { AuthDialogState } from '../auth-modal/auth-dialog-state';
import { AppButtonState } from '../abstract/app-btn-primary/app-btn-state';
import { TwoPartitionModalComponent } from '../abstract/two-partition-modal/two-partition-modal.component'
@Component({
  selector: 'app-auth-modal',
  templateUrl: './auth-modal.component.html',
  styleUrls: ['./auth-modal.component.scss']
})
export class AuthModalComponent implements OnInit {

  _show: boolean;
  _title: string;
  otpSourceId: String;
  confirmationContentText: String;
  authButtonState: AppButtonState;
  dialogState: AuthDialogState;
  @ViewChild('appTwoPartitionModal') appTwoPartitionModal: TwoPartitionModalComponent ;

  @Output() onOTPSourceIdChange = new EventEmitter<String>();
  @Output() authButtonStateEvent = new EventEmitter<AppButtonState>();
  @Output() dialogStateEventEmitter = new EventEmitter<AuthDialogState>();

  authStateEnum = AuthDialogState;

  constructor() {
    this.openSignIn();
  }

  ngOnInit() {
  }

  public open(state: AuthDialogState): void{
    if(state == AuthDialogState.SIGN_IN){
      this.openSignIn();
    }else if(state == AuthDialogState.SIGN_UP){
      this.openSignUp();
    }
    this.appTwoPartitionModal.open()
  }

  onSuccess = (successResults: any) => {
    console.log("success",successResults);
    if(successResults.dialogType === AuthDialogState.SIGN_UP
        || successResults.dialogType === AuthDialogState.SIGN_IN){
      this.changeOtpSourceId(successResults.otpSourceId);
      this.openVerifyOtp();
    }
    else if(successResults.dialogType === AuthDialogState.FORGOT_PASSWORD){
      this.confirmationContentText = "An email has been sent on your registered email id. Please check.";
      this.openConfirmation();
    }
    if(successResults.dialogType === AuthDialogState.RESET_PASSWORD){
      this.confirmationContentText = "You password has been reset successfully! Please login to continue.";
      this.openConfirmation();
    }
  }

  changeOtpSourceId = (value: String) => {
    this.otpSourceId = value;
    this.onOTPSourceIdChange.emit(this.otpSourceId);
  }

  login = () => {
    this.authButtonState = AppButtonState.PROCESSING;
    this.authButtonStateEvent.emit(this.authButtonState);
  }

  openSignIn = () => {
    this.authButtonState = AppButtonState.ENABLED;
    this.changeDialogState(AuthDialogState.SIGN_IN);
    this._title = "Login";
  }

  changeDialogState = (dialogState: AuthDialogState) => {
    this.dialogState = dialogState;
    this.dialogStateEventEmitter.emit(this.dialogState);
  }

  openForgotPassword = () => {
    this.changeDialogState(AuthDialogState.FORGOT_PASSWORD);
    this._title = "Forgot Password";
  }

  openSignUp = () => {
    this.changeDialogState(AuthDialogState.SIGN_UP);
    this._title = "Sign up";
  }

  openVerifyOtp = () => {
    this.changeDialogState(AuthDialogState.VERIFY_OTP);
    this._title = "Verify OTP";
  }

  openConfirmation = () => {
    this.changeDialogState(AuthDialogState.CONFIRMATION);
    this._title = "";
  }

  openResetPassword = () => {
    this.changeDialogState(AuthDialogState.RESET_PASSWORD);
    this._title = "Set new Password";
  }

}
