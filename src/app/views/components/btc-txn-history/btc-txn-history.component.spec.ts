import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtcTxnHistoryComponent } from './btc-txn-history.component';

describe('BtcTxnHistoryComponent', () => {
  let component: BtcTxnHistoryComponent;
  let fixture: ComponentFixture<BtcTxnHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtcTxnHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtcTxnHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
