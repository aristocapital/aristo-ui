import { Component, OnInit } from '@angular/core';
import { Tab, Tabs } from '../abstract/generic-card/tabs';

import { UserDataService } from "../../../services/networkmanager/user-data.services";

@Component({
  selector: 'app-btc-txn-history',
  templateUrl: './btc-txn-history.component.html',
  styleUrls: ['./btc-txn-history.component.scss'],
  providers: [ UserDataService ]
})
export class BtcTxnHistoryComponent implements OnInit {

  tabs: Tabs = new Tabs(["BTC Transaction History"]);
  loader: boolean = false;
  txnHistoryData: object[];

  constructor(private userDataService: UserDataService) { }

  ngOnInit() {
    this.loader = true;
  }

  ngAfterViewInit() {
    this.userDataService.getBTCHistory(0, 10).subscribe((response)=>{
      this.loader = false;
      this.txnHistoryData = response.data;
    });
  }

  getHistory = () => {
    this.loader = true;
    this.userDataService.getBTCHistory(0, 10).subscribe((response)=>{
      this.loader = false;
      this.txnHistoryData = response.data;
    });
  }

}
