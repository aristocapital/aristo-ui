import { Component, OnInit } from '@angular/core';
import { Tab, Tabs } from '../abstract/generic-card/tabs';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { GraphDataService } from "../../../services/networkmanager/graphData.services";

@Component({
  selector: 'app-nav-growth-card',
  templateUrl: './nav-growth-card.component.html',
  styleUrls: ['./nav-growth-card.component.scss'],
  providers : [ GraphDataService ]
})
export class NavGrowthCardComponent implements OnInit {

  tabs: Tabs = new Tabs(["A10 GROWTH"]);
  options: any;
  chart: AmChart;
  loader: boolean = false;

  constructor(private amChartsService: AmChartsService,
              private graphDataService: GraphDataService) { }

  ngOnInit() {
    this.options = this.makeOptions([]);
    this.loader = true;
  }

  ngAfterViewInit() {
    this.chart = this.amChartsService.makeChart('navGrowthChart', this.options);
    this.graphDataService.getNavGrowthData().subscribe((response)=>{
      this.amChartsService.updateChart(this.chart, () => {
        // Change whatever properties you want, add event listeners, etc.
        this.chart.dataProvider = response.data;
        this.loader = false;

      });
    });
  }

  makeOptions = (dataProvider) => {
    return {
      'type': 'serial',
      'theme': 'light',
      'dataProvider': dataProvider,
      'valueAxes': [{
        'axisColor': '#050405',
        'position': 'left',
        'minMaxMultiplier': 1.2,
        'title': 'Price ($)',
        'color':'#050405',
        'axisTitleOffset': 5,
        'gridColor': '#050405',
        'titleColor': '#666'
      }],
      'graphs': [{
        'id': 'g1',
        'balloonText': '[[category]]<br><b><span style=\'font-size:14px;\'>[[value]]</span></b>',
        'bullet': 'round',
        'bulletSize': 1,
        'lineColor': '#3dbdb6',
        'lineThickness': 2,
        'valueField': 'value'
      }],
      'chartCursor': {
        'categoryBalloonDateFormat': 'YYYY',
        'cursorAlpha': 0,
        'valueLineEnabled': true,
        'valueLineBalloonEnabled': true,
        'valueLineAlpha': 0.5,
        'fullWidth': true,
        "cursorColor":"#3dbdb6",
      },
      'dataDateFormat': 'YYYY',
      'categoryField': 'year',
      'categoryAxis': {
        'minPeriod': 'YYYY',
        'parseDates': true,
        "gridThickness": 0,
        "axisColor": '#050405',
        'color': '#666',
        'title': ""
      }
    };
  }

}
