import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavGrowthCardComponent } from './nav-growth-card.component';

describe('NavGrowthCardComponent', () => {
  let component: NavGrowthCardComponent;
  let fixture: ComponentFixture<NavGrowthCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavGrowthCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavGrowthCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
