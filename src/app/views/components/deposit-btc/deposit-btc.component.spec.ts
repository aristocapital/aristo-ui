import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositBtcComponent } from './deposit-btc.component';

describe('DepositBtcComponent', () => {
  let component: DepositBtcComponent;
  let fixture: ComponentFixture<DepositBtcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepositBtcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositBtcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
