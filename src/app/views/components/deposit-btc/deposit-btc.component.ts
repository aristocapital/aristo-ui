import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { PlainModalComponent } from '../abstract/plain-modal/plain-modal.component'
import { UserDataService } from '../../../services/networkmanager/user-data.services';

@Component({
  selector: 'app-deposit-btc',
  templateUrl: './deposit-btc.component.html',
  styleUrls: ['./deposit-btc.component.scss'],
  providers: [ UserDataService ]
})
export class DepositBtcComponent implements OnInit {

  @ViewChild('dialog') dialog: PlainModalComponent;

  depositLoader: boolean = false;
  userBtcAddress: string = "";

  constructor(private userDataService: UserDataService) { }

  ngOnInit() {
    setTimeout(2000, this.getUserBTCDepositAddress());
    // this.getUserBTCDepositAddress();
  }

  open = () => {
    this.dialog.open();
  }

  close = () => {
    this.dialog.close();
  }

  getUserBTCDepositAddress = () => {
    this.depositLoader = true;
    this.userDataService.getBTCDepositAddress().subscribe(
        (response) => {
          this.userBtcAddress = response.data.publicKey;
          this.depositLoader = false;
        }
    );
  }

}
