export class Tab {

  text: string;

  constructor(pText: string){
    this.text = pText;
  }

  getText(): string{
    return this.text;
  }

}

export class Tabs{

  tabList: Array<Tab> = [];
  selected: number = 0;

  constructor(pTabList: Array<string>){
    // this.tabList = [];
    if(pTabList){
      for(let i=0; i<pTabList.length; i++){
        this.tabList.push(new Tab(pTabList[i]));
      }
    }
  }

  setSelected = (tabTitle: string): void => {
    if(this.tabList && tabTitle){
      for (let index = 0; index < this.tabList.length; index++) {
        if( this.tabList[index].getText() === tabTitle){
          this.selected = index;
        }
      }
    }
  }

  getSelected = (): number => {
    return this.selected;
  }

  getTabsCount = () : number => {
    return this.tabList.length;
  }

  setTabs(tabTextList: Array<string>): void{
    if(tabTextList){
      for(let i=0; i<tabTextList.length; i++){
        this.tabList.push(new Tab(tabTextList[i]));
      }
    }
  }
}
