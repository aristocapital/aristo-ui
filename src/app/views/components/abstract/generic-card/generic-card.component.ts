import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Tabs } from './tabs'

@Component({
  selector: 'app-generic-card',
  templateUrl: './generic-card.component.html',
  styleUrls: ['./generic-card.component.scss']
})
export class GenericCardComponent implements OnInit {

  _tabs: Tabs ;
  _showCardDataLoader: boolean = false;
  @Output() tabsChanged = new EventEmitter<Tabs>();

  constructor() {
  }

  ngOnInit() {
  }

  @Input()
  set tabs(value: Tabs){
    this._tabs = value;
    console.log("tabs Changed");
  }

  @Input()
  set loading(value: boolean){
    this._showCardDataLoader = value;
  }

  changeTab = (selectedTabText) => {
    this._tabs.setSelected(selectedTabText);
    this.tabsChanged.emit(this._tabs);
  }

}
