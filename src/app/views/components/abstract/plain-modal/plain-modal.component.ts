import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { GenericModalComponent } from '../generic-modal/generic-modal.component';

@Component({
  selector: 'app-plain-modal',
  templateUrl: './plain-modal.component.html',
  styleUrls: ['./plain-modal.component.scss']
})
export class PlainModalComponent implements OnInit {

  @ViewChild('dialog') dialog: GenericModalComponent;

  constructor() { }

  ngOnInit() {
  }

  public open(): void{
    this.dialog.open();
  }

  public close(): void{
    this.dialog.close();
  }

}
