import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { UserDataService } from '../../../../services/networkmanager/user-data.services';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [UserDataService]
})
export class HeaderComponent implements OnInit {


  isCollapsed: boolean = true;

  constructor(private userDataService: UserDataService,
              private router: Router) { }

  ngOnInit() {
  }

  logout = () => {
    this.userDataService.logout().subscribe(
      (response) => {
        this.router.navigate(['/']);
      }
  );
  }

}
