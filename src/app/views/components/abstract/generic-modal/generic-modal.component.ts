import { Component, OnInit, Input, ViewChild  } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.directive';

@Component({
  selector: 'app-generic-modal',
  templateUrl: './generic-modal.component.html',
  styleUrls: ['./generic-modal.component.scss']
})
export class GenericModalComponent implements OnInit {

  @ViewChild('genericModal') genericModal: ModalDirective;

  constructor() {}

  ngOnInit() {
  }

  public close(): void{
    this.genericModal.hide();
  }

  public open(): void{
    this.genericModal.show();
  }

}
