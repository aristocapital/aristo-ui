import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppBtnPrimaryComponent } from './app-btn-primary.component';

describe('AppBtnPrimaryComponent', () => {
  let component: AppBtnPrimaryComponent;
  let fixture: ComponentFixture<AppBtnPrimaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppBtnPrimaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppBtnPrimaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
