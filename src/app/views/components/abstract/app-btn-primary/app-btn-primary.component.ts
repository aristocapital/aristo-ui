import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppButtonState } from './app-btn-state'

@Component({
  selector: 'app-btn-primary',
  templateUrl: './app-btn-primary.component.html',
  styleUrls: ['./app-btn-primary.component.scss']
})
export class AppBtnPrimaryComponent implements OnInit {

  _text: string;
  _status: AppButtonState;
  @Input() onClick: Function;


  appButtonStatusEnum = AppButtonState;

  constructor() {
  }

  ngOnInit() {
    this._text = this._text || "Button";
    this._status = this._status || AppButtonState.ENABLED;
  }

  onButtonClick(){
    if(this.onClick !== null && this.onClick !== undefined){
      this.onClick();
    }else{
      console.log("onclick is not defined");
    }
  }

  @Input()
  set text(value: string) {
    this._text = value;
  }

  @Input()
  set status(value: AppButtonState) {
    this._status = value;
  }

}
