export enum AppButtonState {
    ENABLED,
    DISABLED,
    PROCESSING
}
