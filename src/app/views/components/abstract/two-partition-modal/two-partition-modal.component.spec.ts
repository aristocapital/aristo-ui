import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoPartitionModalComponent } from './two-partition-modal.component';

describe('TwoPartitionModalComponent', () => {
  let component: TwoPartitionModalComponent;
  let fixture: ComponentFixture<TwoPartitionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwoPartitionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoPartitionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
