import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { GenericModalComponent } from '../generic-modal/generic-modal.component'

@Component({
  selector: 'app-two-partition-modal',
  templateUrl: './two-partition-modal.component.html',
  styleUrls: ['./two-partition-modal.component.scss']
})
export class TwoPartitionModalComponent implements OnInit {

  @ViewChild('appGenericModal') appGenericModal: GenericModalComponent;
  _title: string;

  constructor() {
  }

  ngOnInit() {
  }

  @Input()
  set title(value: string) {
    this._title = value;
  }

  public open(): void{
    this.appGenericModal.open();
  }

}
