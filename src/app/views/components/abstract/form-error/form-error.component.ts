import { Component, OnInit, Input } from '@angular/core';
import { AbstractControlDirective, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-form-error',
  templateUrl: './form-error.component.html',
  styleUrls: ['./form-error.component.scss']
})
export class FormErrorComponent implements OnInit {

  private static readonly errorMessages = {
    'required': () => 'This field is required',
    'minlength': (params) => 'The min number of characters is ' + params.requiredLength,
    'maxlength': (params) => 'The max allowed number of characters is ' + params.requiredLength,
    'pattern': (params) => 'The required pattern is: ' + params.requiredPattern,
    'years': (params) => params.message,
    'countryCity': (params) => params.message,
    'uniqueName': (params) => params.message,
    'telephoneNumbers': (params) => params.message,
    'telephoneNumber': (params) => params.message
  };

  @Input()
  private control: AbstractControlDirective | AbstractControl;

  @Input()
  globalErrorMessage: String;

  ngOnInit() {
  }

  shouldShowErrors(): boolean {
    return this.control &&
      this.control.errors &&
      (this.control.dirty || this.control.touched);
  }

  listOfErrors(): string[] {
    if(this.shouldShowErrors()){
      return Object.keys(this.control.errors)
        .map(field => this.getMessage(field, this.control.errors[field]));
    }else{
      return null;
    }
  }

  showGlobalErrorMessage(): boolean{
    return (this.globalErrorMessage && this.globalErrorMessage.length>0);
  }

  private getMessage(type: string, params: any) {
    console.log(type, " | " ,params)
    return params.message;//this.errorMessages[type](params);
  }

}
