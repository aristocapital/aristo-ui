import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Tab, Tabs } from '../abstract/generic-card/tabs';
import { UserDataService  } from '../../../services/networkmanager/user-data.services';
import { AppButtonState } from '../../components/abstract/app-btn-primary/app-btn-state';
import { UserBalanceDto } from '../../../dto/user-balance.dto';

@Component({
  selector: 'app-buy-sell-nav-card',
  templateUrl: './buy-sell-nav-card.component.html',
  styleUrls: ['./buy-sell-nav-card.component.scss'],
  providers: [UserDataService]
})
export class BuySellNavCardComponent implements OnInit {

  tabs: Tabs = new Tabs(["Buy A10", "Sell A10"]);
  userBalances: UserBalanceDto;
  balancesLoader: boolean = false;
  buyButtonState: AppButtonState = AppButtonState.DISABLED;
  @Output() buyButtonStateEvent = new EventEmitter<AppButtonState>();
  buyLoader: boolean = false;
  buyBtcAmount: number = 0;
  buyError: string;

  sellButtonState: AppButtonState = AppButtonState.DISABLED;
  @Output() sellButtonStateEvent = new EventEmitter<AppButtonState>();
  sellLoader: boolean = false;
  sellNavAmount: number = 0;
  sellError: string;

  constructor(private userDataService: UserDataService) { }

  ngOnInit() {
    this.getUserBalances();
    this.changeBuyButtonState(AppButtonState.DISABLED);
  }


  getUserBalances = () => {
    this.balancesLoader = true;
    this.userDataService.getBalances().subscribe(
        (response) => {
          this.userBalances = new UserBalanceDto(response.data.a10_balance,
                                            response.data.btc_balance,
                                            response.data.a10_price_in_btc);
          this.changeBuyButtonState(AppButtonState.ENABLED);
          this.balancesLoader = false;
        }
    );
  }

  changeBuyButtonState = (newState) => {
    this.buyButtonState = newState;
    this.buyButtonStateEvent.emit(this.buyButtonState);
  }

  buyAmountChange = (value: number) =>{
    this.buyBtcAmount = value;
    if(this.buyBtcAmount === 0){
      this.changeBuyButtonState(AppButtonState.DISABLED);
    }
    else if(this.buyBtcAmount > this.userBalances.btcBalance){
      this.buyError = "You have exceeded your BTC balance";
      this.changeBuyButtonState(AppButtonState.DISABLED);
    }else{
      this.changeBuyButtonState(AppButtonState.ENABLED);
      this.buyError = null;
    }
  }


  buyA10 = () => {
    this.buyLoader = true;
    this.changeBuyButtonState(AppButtonState.PROCESSING);
    this.userDataService.buyA10Token(this.buyBtcAmount).subscribe(
        (response) => {
          this.buyBtcAmount = 0;
          this.changeBuyButtonState(AppButtonState.DISABLED);
          this.buyLoader = false;
          alert("Your buy A10 request has been received. Thanks");
        }
    );
  }

  changeSellButtonState = (newState) => {
    this.sellButtonState = newState;
    this.sellButtonStateEvent.emit(this.sellButtonState);
  }

  sellAmountChange = (value: number) =>{
    this.sellNavAmount = value;
    if(this.sellNavAmount === 0){
      this.changeSellButtonState(AppButtonState.DISABLED);
    }
    else if(this.sellNavAmount > this.userBalances.a10Balance){
      this.sellError = "You have exceeded your A10 balance";
      this.changeSellButtonState(AppButtonState.DISABLED);
    }else{
      this.changeSellButtonState(AppButtonState.ENABLED);
      this.sellError = null;
    }
  }


  sellA10 = () => {
    this.sellLoader = true;
    this.changeSellButtonState(AppButtonState.PROCESSING);
    this.userDataService.sellA10Token(this.sellNavAmount).subscribe(
        (response) => {
          this.sellNavAmount = 0;
          this.sellLoader = false;
          this.changeSellButtonState(AppButtonState.DISABLED);
          alert("Your Sell A10 request has been received. Thanks");
        }
    );
  }

}
