import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuySellNavCardComponent } from './buy-sell-nav-card.component';

describe('BuySellNavCardComponent', () => {
  let component: BuySellNavCardComponent;
  let fixture: ComponentFixture<BuySellNavCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuySellNavCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuySellNavCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
