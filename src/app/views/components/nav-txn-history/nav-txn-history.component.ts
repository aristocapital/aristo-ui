import { Component, OnInit } from '@angular/core';
import { Tab, Tabs } from '../abstract/generic-card/tabs';

import { UserDataService } from "../../../services/networkmanager/user-data.services";
@Component({
  selector: 'app-nav-txn-history',
  templateUrl: './nav-txn-history.component.html',
  styleUrls: ['./nav-txn-history.component.scss'],
  providers: [ UserDataService ]
})
export class NavTxnHistoryComponent implements OnInit {

  tabs: Tabs = new Tabs(["A10 Transaction History"]);
  loader: boolean = false;
  txnHistoryData: object[];

  constructor(private userDataService: UserDataService) { }

  ngOnInit() {
    this.loader = true;
  }

  ngAfterViewInit() {
    this.getHistory();
  }

  getHistory = () => {
    this.loader = true;
    this.userDataService.getA10History(0, 10).subscribe((response)=>{
      this.loader = false;
      this.txnHistoryData = response.data;
    });
  }

}
