import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavTxnHistoryComponent } from './nav-txn-history.component';

describe('NavTxnHistoryComponent', () => {
  let component: NavTxnHistoryComponent;
  let fixture: ComponentFixture<NavTxnHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavTxnHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavTxnHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
