import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserBalanceCardComponent } from './user-balance-card.component';

describe('UserBalanceCardComponent', () => {
  let component: UserBalanceCardComponent;
  let fixture: ComponentFixture<UserBalanceCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserBalanceCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserBalanceCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
