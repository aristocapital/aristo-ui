import { Component, OnInit } from '@angular/core';
import { Tab, Tabs } from '../abstract/generic-card/tabs';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { GraphDataService } from "../../../services/networkmanager/graphData.services";

@Component({
  selector: 'app-user-balance-card',
  templateUrl: './user-balance-card.component.html',
  styleUrls: ['./user-balance-card.component.scss'],
  providers : [ GraphDataService ]
})
export class UserBalanceCardComponent implements OnInit {

  tabs: Tabs = new Tabs(["CURRENT PORTFORLIO DISTRIBUTION"]);
  options: any;
  chart: AmChart;
  loader: boolean = false;

  constructor(private amChartsService: AmChartsService,
              private graphDataService: GraphDataService) { }

  ngOnInit() {
    this.options = this.makeOptions([]);
    this.loader = true;
  }

  ngAfterViewInit() {
    this.chart = this.amChartsService.makeChart('navDistributionChart', this.options);
    this.graphDataService.getNavCompositionData().subscribe((response)=>{
      this.amChartsService.updateChart(this.chart, () => {
        // Change whatever properties you want, add event listeners, etc.
        this.chart.dataProvider = response.data;
        this.loader = false;
      });
    });
  }

  makeOptions = (dataProvider) => {
    return {
        "type": "pie",
        "startDuration": 0,
        "theme": "light",
        "addClassNames": true,
        "labelsEnabled": true,
        "legend":{
          "enabled": false,
          "divId": "chartLegends",
         	"position":"bottom",
          "marginBottom":16,
          "autoMargins":false,
          "markerLabelGap": 3,
          "markerType": "line",
          "markerBorderThickness": 3,
          "markerBorderAlpha": 0.7,
          "markerSize": 16,
          "horizontalGap": 1,
          "useMarkerColorForLabels": true
        },
        "labelRadius": 5,
        "radius": "42%",
        "innerRadius": "60%",
        "dataProvider": dataProvider,
        "valueField": "value",
        "titleField": "name"
      }
  }

}
