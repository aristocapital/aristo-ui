import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { PlainModalComponent } from '../abstract/plain-modal/plain-modal.component'
import { LocalStorageService } from '../../../services/localstorage/local-storage.service'

@Component({
  selector: 'app-kyc',
  templateUrl: './kyc.component.html',
  styleUrls: ['./kyc.component.scss']
})
export class KycComponent implements OnInit {

  @ViewChild('dialog') dialog: PlainModalComponent;

  countrySelector: boolean = true;
  selectedCountry: string ;//= false;
  countryList: Array<string> = ["USA", "Russia", "Japan"];
  kycForm: FormGroup;

  documents: any = {
    aadharFrontUrl: null,
    aadharBackUrl: null,
    pancardUrl: null,
    incomeSourceUrl: null
  };
  constructor() {
  }

  ngOnInit() {
    this.kycForm = new FormGroup ({
      selectedCountry: new FormControl('',Validators.required)
    })
  }

  open = () => {
    this.dialog.open();
  }

  close = () => {
    this.dialog.close();
  }

  onCountryChange(newValue: string): void{
  }

  onFileSelect = (event: any, docType: string) => {
    console.log("onFileSelect",docType, event);
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event:any) => {
        switch(docType){
          case 'aadharFrontUrl':
            this.documents.aadharFrontUrl = event.target.result;
            break;
          case 'aadharBackUrl':
            this.documents.aadharBackUrl = event.target.result;
            break;
          case 'pancardUrl':
            this.documents.pancardUrl = event.target.result;
            break;
          case 'incomeSourceUrl':
            this.documents.incomeSourceUrl = event.target.result;
            break;
        }
        // Object.assign(this.documents, {docType.valueOf(): event.target.result});
        console.log("documents Url", this.documents);
      }

      reader.readAsDataURL(event.target.files[0]);
    }
    // this.aadharFront = someinput.target.files[0];
  }

}
