
export class UserBalanceDto{
  a10Balance: number;
  btcBalance: number;
  a10PriceInBtc: number;

  constructor(_a10Balance: number,
              _btcBalance: number,
              _a10PriceInBtc: number){
    this.a10Balance = _a10Balance;
    this.btcBalance = _btcBalance;
    this.a10PriceInBtc = _a10PriceInBtc;
  }

}
